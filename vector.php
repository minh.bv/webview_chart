<?php

$data=[];
if(isset($_POST['submit'])){
    $excep='';
    if(isset($_FILES["file"]["type"])){
        $temporary = explode(".", $_FILES["file"]["name"]);
        $file_extension = end($temporary);
        if ($_FILES["file"]["type"] == "text/plain" && $_FILES["file"]["size"] < 100000) {
            if ($_FILES["file"]["error"] > 0){
                // echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
            }else{
                if (file_exists("upload/" . $_FILES["file"]["name"])) {
                    $excep=$excep.$_FILES["file"]["name"] . " file has the same name";
                }else{
                    $sourcePath = $_FILES['file']['tmp_name'];// Storing source path of the file in a variable
                    $targetPath = "upload/".$_FILES['file']['name']; // Target path where file is to be stored
                    move_uploaded_file($sourcePath,$targetPath) ;

                    
                }
            }
        }else{
            $excep= ','.$excep.",File execption!";
        }
    }
    $fp = @fopen($targetPath, "r");
  
// Kiểm tra file mở thành công không

$i=0;
if (!$fp) {
    // echo 'mở file fail';
}
else
{
    // Lặp qua từng dòng để đọc
    while(!feof($fp))
    {
         $data[$i]=fgets($fp);
         $i++;
    }
}
}else{
    $fp = @fopen('upload/vector.txt', "r");
  
// Kiểm tra file mở thành công không

$i=0;
if (!$fp) {
    // echo 'mở file fail';
}
else
{
    // Lặp qua từng dòng để đọc
    while(!feof($fp))
    {
         $data[$i]=fgets($fp);
         $i++;
    }
}
}

// $char = new api();
// $data=$char->vector();
// var_dump($data);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
    #chart1 {
    min-width: 30px;
    max-width: 600px;
    height: 500px;
    margin: 1em auto;
}

#csv {
	display: none;
}
</style>
</head>
<body>
<div class="container">
    <div class="row">
    <div class="alert alert-danger" role="alert">
  <?php if(isset($excep)){echo $excep;}else{echo 'upload a .txt file with content format [x,y,lenght,dict] demo: [3.6,190,49]';} ?>
</div>
        <form action="" method="post" enctype="multipart/form-data">
        <div class="col-md-8">
        <input type="file" name="file" id="" class="form-control">
        </div>
        <input type="submit" name="submit" class="btn btn-success" value="upload">
    </form>
    </div>
    </div>

    <hr>
<div id="chart1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/vector.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>$(function () {
    Highcharts.chart('chart1', {

title: {
    text: 'Highcharts Vector'
},
xAxis: {
    min: 0,
    max: 100,
    gridLineWidth: 1
},
yAxis: {
    min: 0,
    max: 100
},
series: [{
    type: 'vector',
    name: 'vector field',
    color: Highcharts.getOptions().colors[1],
    data: [
        <?php foreach ($data as $val){
            echo $val.",";
        } ?>

        
        
    ]
}]

})
});</script>


    
</body>
</html>